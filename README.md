//#define My_Debug

using Game.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class Form1 : Form
    {
        const int FrameNum = 5;
        const int SplatNum = 3;
        bool splat = false;
        int _gameFrame = 0;
        int _splatTime = 0;

        int _hits = 0;
        int _misses = 0;
        int _totalShots = 0;
        double _averageHits = 0;

#if My_Debug

        int _cursX = 0;
        int _cursY = 0;
#endif
        CMyxa _mole;
        CSplat _splat;
        CSign _sign;
        CScoreFrame _scoreFrame;

        Random rnd = new Random();

        public Form1()
        {
            InitializeComponent();

            Bitmap b = new Bitmap(Resources.Site);
            this.Cursor = CustomCursor.CreateCursor(b, b.Height / 2, b.Width / 2);

            _scoreFrame = new CScoreFrame() { Left = 10, Top = 10 };
            _sign = new CSign() { Left = 380, Top = 50 };
            _mole = new CMyxa() { Left = 10, Top = 200 };
            _splat = new CSplat();
        }

        private void timerGameLoop_Tick(object sender, EventArgs e)
        {
            if (_gameFrame >= FrameNum)
            {
                UpdateMyxa();
                _gameFrame = 0;
            }

            if (splat)
            {
                if (_splatTime >=SplatNum)
                {
                    splat = false;
                    _splatTime = 0;
                    UpdateMyxa();
                }

                _splatTime++;
            }

            _gameFrame++;
            this.Refresh();
        }

        private void UpdateMyxa()
        {
            _mole.Update(
                rnd.Next(Resources.Myxa.Width, this.Width - Resources.Myxa.Width),
                rnd.Next(this.Height / 2, this.Height - Resources.Myxa.Height * 2)
            );
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics dc = e.Graphics;

            if (splat == true)
            {
                _splat.DrawImage(dc);
            }
            else
            {
                _mole.DrawImage(dc);
            }
           

            _sign.DrawImage(dc);
            _scoreFrame.DrawImage(dc);
#if My_Debug

            TextFormatFlags flags = TextFormatFlags.Left | TextFormatFlags.EndEllipsis;
            Font _font = new System.Drawing.Font("Stencil", 12, FontStyle.Regular);
            TextRenderer.DrawText(dc, "X=" + _cursX.ToString() + ":" + "Y=" + _cursY.ToString(),_font,
                new Rectangle(0, 0, 120, 20), SystemColors.ControlText, flags);
#endif
            // Screen
            TextFormatFlags flags = TextFormatFlags.Left;
            Font _font = new System.Drawing.Font("Stencil", 12, FontStyle.Regular);
            TextRenderer.DrawText(e.Graphics, "Shots:" + _totalShots.ToString(), _font, new Rectangle(60, 92, 120, 20),
                SystemColors.ControlText, flags);
            TextRenderer.DrawText(e.Graphics, "Hits:" + _hits.ToString(), _font, new Rectangle(60, 112, 120, 20),
                SystemColors.ControlText, flags);
            TextRenderer.DrawText(e.Graphics, "Misses:" + _misses.ToString(), _font, new Rectangle(60, 132, 120, 20),
                SystemColors.ControlText, flags);
            TextRenderer.DrawText(e.Graphics, "Avg:" + _averageHits.ToString("F0") + "%", _font,
                new Rectangle(60, 152, 120, 20), SystemColors.ControlText, flags);




            base.OnPaint(e);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
#if My_Debug
            _cursX = e.X;
            _cursY = e.Y;
#endif
            this.Refresh();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.X > 407 && e.X < 445 && e.Y > 108 && e.Y < 126) // Start
            {
                timerGameLoop.Start();
            }
            else if (e.X > 500 && e.X < 534 && e.Y > 108 && e.Y < 126) // Stop
            {
                timerGameLoop.Stop();
            }
            else if (e.X > 405 && e.X < 450 && e.Y > 174 && e.Y < 193) // Reset
            {
                Application.Restart();

            }
            else if (e.X > 500 && e.X < 534 && e.Y > 174 && e.Y < 193) // Quit
            {
                timerGameLoop.Stop();
                DialogResult result = MessageBox.Show(this, "Exit?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    this.Close();
                }
            }
            else
            {
                if (_mole.Hit(e.X, e.Y))
                {
                    splat = true;
                    _splat.Left = _mole.Left - Resources.Splat.Width / 3;
                    _splat.Top = _mole.Top - Resources.Splat.Height / 3;

                    _hits++;
                }
                else
                {
                    _misses++;
                }
                _totalShots = _hits + _misses;
                _averageHits = (double)_hits / (double)_totalShots * 100.0;
            }


            FireGun();
        }

        private void FireGun()
        {

            SoundPlayer simpleSound = new SoundPlayer(Resources.Shot);

            simpleSound.Play();
        }
    }
}
